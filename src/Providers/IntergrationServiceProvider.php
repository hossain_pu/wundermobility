<?php

namespace WunderMobility\Providers;
use Pimple\{Container,ServiceProviderInterface};
use WunderMobility\Providers\Intergration;

class IntergrationServiceProvider implements ServiceProviderInterface
{
    /**
     * Generate Api service
     *
     * @param Container $app
     * @return Intergration
     */
    public function register(Container $app)
    {
        $app['api'] = $app->factory(function(){
            return new Intergration();
        });
    }
}