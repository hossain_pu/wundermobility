<?php

namespace WunderMobility\Providers;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Form\Extension\Core\Type\{FormType,TextType,SubmitType,TelType,HiddenType,NumberType};
use Symfony\Component\HttpFoundation\{Request,Response};
use Symfony\Component\VarDumper\VarDumper;

/**
 * Registration process
 */
class RegistrationProvider implements ControllerProviderInterface
{

    private $app;
    private $cookieName = 'registration';
    private $cookie;
    private $userid = 0;

    /**
     * Main Router
     *
     * @param Application $app
     */
    public function connect(Application $app)
    {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];
        $controllers->match('/', function (Request $request) use ($app) {
            $this->app = $app;
            if ($this->cookie = $this->getCookie($request)) {
                #var_dump($this->cookie);
                $this->userid = $this->cookie['id'];
                return $this->{'step' . $this->cookie['page']}($request, $app);
            }

            if (empty($request->request->get('form'))) {
                return $this->stepone($request, $app);
            }

            return $this->{'step' . $request->request->get('form')['page']}($request, $app);
        });

        return $controllers;
    }

    /**
    * @return Boolean
    */
    private function isSetUserId()
    {
        if ($this->userid > 0) {
            return true;
        }
        return false;
    }

    /**
     * Step one of Registration
     *
     * @param Request $request
     * @return Respond
     */
    private function stepone(Request $request)
    {
        #  return new Response('im here');
        $form = $this->app['form.factory']->createBuilder(FormType::class)
            ->add('firstname', TextType::class, ['label' => 'Firstname: ' ,'attr'   => ['class' =>'form-control']])
            ->add('lastname', TextType::class, ['label' => 'Lastname: ','attr'   => ['class' =>'form-control']])
            ->add('telephone', NumberType::class, ['label' => 'Telephone: ','attr'   => ['class' =>'form-control']])
            ->add('page', HiddenType::class, ['data' => 'one'])
            ->add('submit', SubmitType::class, ['label' => 'Next','attr'   => ['class' =>'btn btn-secondary']])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if ($this->isSetUserId()) {
                $sql = "UPDATE users SET firstname = ?,lastname = ?,telephone = ? WHERE id = ?";
                $this->app['db']->executeUpdate($sql, [
                    $data['firstname'],
                    $data['lastname'],
                    $data['telephone'],
                    (int) $this->userid,
                ]);
            } else {
                $this->app['db']->insert('users', [
                    'firstname' => $data['firstname'],
                    'lastname' => $data['lastname'],
                    'telephone' => $data['telephone'],
                ]
                );
                $this->userid = $this->app['db']->lastInsertId();
            }
            return $this->steptwo(new Request);
        }

        $this->setCookie(['page' => 'one', 'id' => $this->userid]);
        return $this->app['twig']->render('templates\index.twig', array('form' => $form->createView()));
    }

    /**
     * Step Two of Registration
     *
     * @param Request $request
     * @return Respond
     */
    private function steptwo(Request $request)
    {
        $form = $this->app['form.factory']->createBuilder(FormType::class)
            ->add('street', TextType::class, ['label' => 'Street: ','attr'   => ['class' =>'form-control']])
            ->add('housenumber', TextType::class, ['label' => 'House Number: ','attr'   => ['class' =>'form-control']])
            ->add('zipcode', NumberType::class, ['label' => 'Zipcode: ','attr'   => ['class' =>'form-control']])
            ->add('city', TextType::class, ['label' => 'City: ','attr'   => ['class' =>'form-control']])
            ->add('page', HiddenType::class, ['data' => 'two'])
            ->add('submit', SubmitType::class, ['label' => 'Next','attr'   => ['class' =>'btn btn-secondary btn-lg float-right']])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if ($this->isSetUserId()) {
                $sql = "UPDATE users SET street = ?,housenumber = ?,zipcode = ?,city = ? WHERE id = ?";
                $this->app['db']->executeUpdate($sql, array(
                    $data['street'],
                    $data['housenumber'],
                    $data['zipcode'],
                    $data['city'],
                    (int) $this->userid,
                ));
            } else {
                $this->app['db']->insert('users', [
                    'street' => $data['street'],
                    'housenumber' => $data['housenumber'],
                    'zipcode' => $data['zipcode'],
                    'city' => $data['city'],
                ]
                );
            }
            return $this->stepthree(new Request);
        }
        $this->setCookie(['page' => 'two', 'id' => $this->userid]);
        return $this->app['twig']->render('templates\index.twig', array('form' => $form->createView()));
    }

    /**
     * Step Three of Registration
     *
     * @param Request $request
     * @return Respond
     */
    private function stepthree(Request $request)
    {
        $form = $this->app['form.factory']->createBuilder(FormType::class)
            ->add('accountowner', NumberType::class, ['label' => 'Account Owner: ','attr'   => ['class' =>'form-control']])
            ->add('iban', NumberType::class, ['label' => 'IBAN: ','attr'   => ['class' =>'form-control']])
            ->add('page', HiddenType::class, ['data' => 'three'])
            ->add('submit', SubmitType::class, ['label' => 'Submit','attr'   => ['class' =>'btn btn-secondary']])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $request = ['body' => json_encode([
                'customerId' => $this->userid,
                'iban' => $data['iban'],
                'owner' => $data['accountowner'],
            ]),
            ];

            $paymentDataId = $this->sendPaymentData($request);

            if (array_key_exists('error', $paymentDataId)) {
                $message = 'Error: ' . $paymentDataId['error'];
                $data['paymentdataid'] = null;
            } else {
                $message = 'Your payment data id is: ';
                $message .= $data['paymentdataid'] = $paymentDataId['paymentDataId'];
            }

            if ($this->isSetUserId()) {
                $sql = "UPDATE users SET accountowner = ?,iban = ? , paymentdataid = ? WHERE id = ?";
                $this->app['db']->executeUpdate($sql, [
                    $data['accountowner'],
                    $data['iban'],
                    $data['paymentdataid'],
                    (int) $this->userid,
                ]);
            } else {
                $this->app['db']->insert('users', [
                    'accountowner' => $data['accountowner'],
                    'iban' => $data['iban'],
                    'paymentdataid' => $data['paymentdataid'],
                ]
                );
            }

            return $this->stepfour($message);
        }

        $this->setCookie(['page' => 'three', 'id' => $this->userid]);
        return $this->app['twig']->render('templates\index.twig', array('form' => $form->createView()));
    }

    /**
     * Step Four of Registration
     *
     * @param String $message
     * @return Respond
     */
    private function stepfour(String $message)
    {
        $this->clearCookie();
        return $this->app['twig']->render('templates\greeting.twig', ['message' => $message]);
    }

    /**
     *
     * @param array $cookeValue
     * @return void
     */
    private function setCookie(array $cookeValue)
    {
        $response = new Response();
        $this->cookie = new Cookie($this->cookieName, serialize($cookeValue), time() + 31556926, '/');
        $response->headers->setCookie($this->cookie);
        $response->send();
    }

    /**
     *
     * @param Request $request
     * @return Array/NULL
     */
    private function getCookie(Request $request)
    {
        return $request->cookies->get($this->cookieName) ? unserialize($request->cookies->get($this->cookieName)) : null;
    }

    /**
     *
     * @return void
     */
    private function clearCookie()
    {
        $response = new Response();
        $response->headers->clearCookie($this->cookieName);
        $response->send();
    }

    /**
     *
     * @param array $data
     * @return Array
     */
    private function sendPaymentData(Array $data)
    {
        return $this->app['api']->clientRequest($data);
    }

}
