<?php
use Symfony\Component\HttpFoundation\{Response,Request};
use Silex\Application;
use WunderMobility\Providers\RegistrationProvider;

$app->get('/',function(Request $request,Application $app){
return $app->redirect('/register');
})->bind('home');


/* $app->get('/register',function(Request $request,Application $app){
    return new Response('Registerpage');

})->bind('registration'); */

$app->mount('/register', new RegistrationProvider);