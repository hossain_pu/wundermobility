<?php
use Silex\Application;
use Silex\Provider\{DoctrineServiceProvider,FormServiceProvider,TwigServiceProvider,LocaleServiceProvider,TranslationServiceProvider};

require __DIR__ . '/../vendor/autoload.php';
$app = new Application([
    'debug' => true,
]);

/**
 * Register Doctrine
 */
$app->register(new DoctrineServiceProvider, [
    'db.options' => [
        'driver' => 'pdo_sqlite',
        'user' => 'hossein',
        'password' => 'Life!sBeutiful',
        'path' => __DIR__ . '/../db/wbregistration.s3db',
    ],
]);

/**
 * Register FormBuilder
 */
$app->register(new FormServiceProvider);

/**
 * Register Translate + Locale service
 */
$app->register(new LocaleServiceProvider());
$app->register(new TranslationServiceProvider(), array('locale_fallbacks' => array('en'),
));

/**
 * Register Twig
 */
$app->register(new TwigServiceProvider, [
    'twig.path' => __DIR__ . '/../views',

]);

/**
 * Register Validator
 */
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\VarDumperServiceProvider());

/**
 * Register own intergration service
 */
$app->register(new WunderMobility\Providers\IntergrationServiceProvider);

/**
 * Addin Routs
 */
require __DIR__ . '/../routes/web.php';
