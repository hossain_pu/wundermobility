<?php
namespace WunderMobility\Providers;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * To Intergrate with WunderMobility Api
 */
class Intergration
{

    private $baseUri = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
    private $client;

    public function __construct()
    {

        $this->client = new Client([
            'base_uri' => $this->baseUri,
            'headers' => ['Content-Type' => 'application/json'],
            RequestOptions::ALLOW_REDIRECTS => [
                'max'             => 10,
                'strict'          => true,
                'referer'         => true,
                'track_redirects' => true,
            ]
        ]);
    }

    /**
     *
     * @param String $baseUri
     */
    public function setBaseUrl(String $baseUri){
        $this->baseUri = $baseUri;
    }

    /**
     *
     * @param Array $request
     * @param String $url
     * @param String $method
     * @return Array
     */
    public function clientRequest(Array $request,String $url = '',String $method = 'POST')
    {

        if (empty($request['body']) ) {
            return;
        }
        $request['contentType'] = $request['contentType'] ?? '\'Content-Type\' => \'application/json\'';

        try {
            $response = $this->client->request($method,
            $url,
            $request);
            return  json_decode($response->getBody()->getContents(), true);
        }
        catch (\Exception $e) {
             return ['error'=>$e->getMessage()] ;
        }

    }

}
